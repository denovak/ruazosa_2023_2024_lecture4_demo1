package hr.fer.ruazosa.lecture4.navigationsample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import hr.fer.ruazosa.lecture4.navigationsample.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        var mainLayout = ActivityMainBinding.inflate(layoutInflater)
        setContentView(mainLayout.root)
    }
}