package hr.fer.ruazosa.lecture4.navigationsample

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hr.fer.ruazosa.lecture4.navigationsample.databinding.FragmentSecondBinding

class SecondFragment : Fragment() {
    var _binding: FragmentSecondBinding? = null

    val binding get () = _binding !!
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        val fragmentView = binding.root
        binding.secondFragmentTextView.text = arguments?.getString("STRING_TO_WRITE_ON_SCREEN","")
        return fragmentView
    }
    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}