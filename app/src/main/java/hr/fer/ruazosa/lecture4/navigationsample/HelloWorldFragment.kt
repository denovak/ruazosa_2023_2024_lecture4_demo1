package hr.fer.ruazosa.lecture4.navigationsample

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import hr.fer.ruazosa.lecture4.navigationsample.databinding.FragmentHelloWorldBinding


class HelloWorldFragment : Fragment() {

    private var _binding: FragmentHelloWorldBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentHelloWorldBinding.inflate(inflater, container, false)
        val view = binding.root
        binding.clickToSeeOtherFragmentButton.setOnClickListener {
            val navController = findNavController()
            val secondFragmentParams = Bundle()
            secondFragmentParams.putString("STRING_TO_WRITE_ON_SCREEN", "Hello world from second fragment")
            navController.navigate(R.id.action_helloWorldFragment_to_secondFragment, secondFragmentParams)
        }
        return view
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}